package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CheckOut extends AppCompatActivity {

    TextView tvTujuan, tvTanggal1, tvWaktu1, tvTanggal2, tvWaktu2, tvTiket, tvHargaTotal;
    Button btnKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        tvTujuan = (TextView) findViewById(R.id.tv_tujuan);
        tvTanggal1 = (TextView) findViewById(R.id.tv_tanggal_kbrkt);
        tvWaktu1 = (TextView) findViewById(R.id.tv_waktu_kbrkt);
        tvTanggal2 = (TextView) findViewById(R.id.tv_tanggal_pulang);
        tvWaktu2 = (TextView) findViewById(R.id.tv_waktu_pulang);
        tvTiket = (TextView) findViewById(R.id.tv_jumlah_tiket);
        tvHargaTotal = (TextView) findViewById(R.id.harga);

        Bundle bundle = getIntent().getExtras();
        String tujuan = bundle.get("tujuan").toString();
        String tanggal1 = bundle.get("tanggal").toString();
        String waktu1 = bundle.get("waktu").toString();
        String tanggal2 = bundle.get("tanggal2").toString();
        String waktu2 = bundle.get("waktu2").toString();
        String tiket = bundle.get("tiket").toString();
        String hargaTotal = bundle.get("hargaTotal").toString();

        tvTujuan.setText(tujuan);
        tvTanggal1.setText(tanggal1);
        tvWaktu1.setText(waktu1);
        tvTanggal2.setText(tanggal2);
        tvWaktu2.setText(waktu2);
        tvTiket.setText(tiket);
        tvHargaTotal.setText(hargaTotal);

        btnKonfirmasi = (Button) findViewById(R.id.btn_konfirmasi);
        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CheckOut.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}
